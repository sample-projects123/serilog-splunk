using Serilog;
using SerilogSplunk;

try
{
    var builder = WebApplication.CreateBuilder(args);
    var rnd = new Random();
    Log.Logger = new LoggerConfiguration()
        .ReadFrom.Configuration(builder.Configuration)
        .CreateLogger();

    // Add services to the container.
    builder.Host.UseSerilog();

    builder.Services.AddEndpointsApiExplorer();
    builder.Services.AddSwaggerGen();

    var app = builder.Build();

    // Configure the HTTP request pipeline.
    if (app.Environment.IsDevelopment())
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }

    app.UseHttpsRedirection();

    app.UseSerilogRequestLogging();

    app.MapGet("/get-something", (ILogger<Program> _logger) =>
    {
        _logger.LogInformation("All entities have been listed.");
        return TypedResults.Ok();
    });
    app.MapGet("/get-something-by-id", (int id, ILogger<Program> _logger) =>
    {
        if (rnd.Next(10) < 5)
            _logger.LogInformation("Displayed information about the entity with the id: {id}.", id);
        else
            _logger.LogWarning("Entity with id: {id} doesn't exist.", id);

        return TypedResults.Ok();
    });
    app.MapPost("/post-something", (SomeEntity entity, ILogger<Program> _logger) =>
    {
        Log.Information("Entity was created. {@entity}", entity);
        _logger.LogInformation("Entity was created. {@entity}", entity);
        return TypedResults.Ok();
    });
    app.MapPut("/update-something", (SomeEntity entity, ILogger<Program> _logger) =>
    {
        _logger.LogInformation("Entity was updated. {@entity}", entity);
        return TypedResults.Ok();
    });
    app.MapDelete("/delete-something", (int id, ILogger<Program> _logger) =>
    {
        if (rnd.Next(10) < 5)
            _logger.LogInformation("Entity with id: {id} was deleted.", id);
        else
            _logger.LogWarning("Access to entity with id: {id} is denied.", id);

        return TypedResults.Ok();
    });

    app.Run();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Unhandled exception.");
}
finally
{
    Log.Information("Shut down complete.");
    Log.CloseAndFlush();
}

