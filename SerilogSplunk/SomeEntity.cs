﻿namespace SerilogSplunk;

public class SomeEntity
{
    public int Id { get; set; }
    public string Name { get; set; }
    public DateOnly Date { get; set; }
}
